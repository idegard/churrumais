  <%@ include file="header.jsp" %>
  <div id="content">
   <h2>Super Proyecto <span style="color:#B29B35;"> Churrumais</span></h2>
    <blockquote>Este es el super proyectazo churrumais hecho con spring, hibernate y jsp</blockquote>
  	  
  	  <c:if test="${not empty lista}">
 
		<ul>
			<c:forEach var="listValue" items="${lista}">
			 <h2>${listValue.titulo}</h2>
    <p class="info"><img class="noborder" src="img/flower.jpg" alt="flower" title="flower"/> </p>
    <p>${listValue.contenido}</p>
    <p class="post"> <a href="#" class="readmore">Read more</a> <a href="#" class="comments">Comments (3)</a> <span class="date">April 26, 2007</span> </p>
			</c:forEach>
		</ul>
 
	</c:if>
   
    </div>
  <div id="container-foot">
    <div id="footer">
      <p><a href="#">homepage</a> | <a href="mailto:denise@mitchinson.net">contact</a> | &copy; 2007 Anyone | Design by <a href="http://www.mitchinson.net"> www.mitchinson.net</a> | Licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 License</a></p>
    </div>
  </div>
</div>
</body>
</html>
