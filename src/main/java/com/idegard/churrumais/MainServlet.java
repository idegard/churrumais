package com.idegard.churrumais;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.idegard.churrumais.model.Noticia;
import com.idegard.churrumais.repository.NoticiaRepository;

/**
 * Servlet implementation class MainServlet
 */

@Component
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private NoticiaRepository noticiaRepository;
    
	
	  public void init(ServletConfig config) {
	    try {
			super.init(config);
			 SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,config.getServletContext());
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	  }
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<Noticia> arregloNoticias= new ArrayList<Noticia>();
		for(Noticia noticia :noticiaRepository.findAll()){
			arregloNoticias.add(noticia);
		}
		request.setAttribute("lista", arregloNoticias);
		request.getRequestDispatcher("/main.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
