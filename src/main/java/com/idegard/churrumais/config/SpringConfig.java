/*
 * Copyright 2012 SURFnet bv, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.idegard.churrumais.config;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.validation.Validator;

/**
 * The SpringConfiguration is a {@link Configuration} that can be overridden if
 * you want to plugin your own implementations. Note that the two most likely
 * candidates to change are the {@link AbstractAuthenticator} an
 * {@link AbstractUserConsentHandler}. You can change the implementation by
 * editing the application.apis.properties file where the implementations are
 * configured.
 */


/*
 * The component scan can be used to add packages and exclusions to the default
 * package
 */

//@ImportResource("classpath:spring-repositories.xml")
@Configuration
@ComponentScan(basePackages = {"com.idegard.churrumais"})
@EnableJpaRepositories("com.idegard.churrumais.repository")
@EnableTransactionManagement
@EnableScheduling
public class SpringConfig {
  
  
  @Bean
  public javax.sql.DataSource dataSource() {
    DataSource dataSource = new DataSource();
    dataSource.setDriverClassName("com.mysql.jdbc.Driver");
    dataSource.setUrl("jdbc:mysql://idegard.com:3306/churru");
    dataSource.setUsername("churru");
    dataSource.setPassword("churru");
    dataSource.setTestOnBorrow(true);
    dataSource.setValidationQuery("SELECT 1");
    return dataSource;
  }
 
  @Bean
  public JpaTransactionManager transactionManager() {
    return new JpaTransactionManager();
  }
  
  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    final LocalContainerEntityManagerFactoryBean emfBean = new LocalContainerEntityManagerFactoryBean();
    emfBean.setDataSource(dataSource());
    emfBean.setPersistenceUnitName("IdegardPersistence");
    emfBean.setPersistenceProviderClass(org.hibernate.ejb.HibernatePersistence.class);
    return emfBean;
  }
      
  @Bean
  public Validator validator() {
    // This LocalValidatorFactoryBean already uses the SpringConstraintValidatorFactory by default,
    // so available validators will be wired automatically.
    return new org.springframework.validation.beanvalidation.LocalValidatorFactoryBean();
  }
 
}