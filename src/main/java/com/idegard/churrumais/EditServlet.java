package com.idegard.churrumais;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.idegard.churrumais.model.Noticia;
import com.idegard.churrumais.repository.NoticiaRepository;

/**
 * Servlet implementation class MainServlet
 */

@Component
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private NoticiaRepository noticiaRepository;
    
	
	  public void init(ServletConfig config) {
	    try {
			super.init(config);
			 SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,config.getServletContext());
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	  }
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/edit.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Noticia noti = new Noticia();
		noti.setTitulo(request.getParameter("titulo"));
		noti.setContenido(request.getParameter("contenido"));
		noticiaRepository.save(noti);
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/servlet"));
		
	}

}
